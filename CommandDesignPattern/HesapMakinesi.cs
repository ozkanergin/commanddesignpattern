﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern
{
    class HesapMakinesi
    {
        public static int Islemi_Yap(Islem_Turu islem_turu , int ilk_sayi , int ikinci_sayi)     // Bu turlu yaparsak her islemde else if eklicez
        {
            int sonuc = 0;

            if(islem_turu == Islem_Turu.TOPLAM)
            {
                sonuc = ikinci_sayi + ilk_sayi;
            }else if(islem_turu == Islem_Turu.CIKARMA)
            {
                sonuc = ilk_sayi - ikinci_sayi;
            }else if(islem_turu == Islem_Turu.CARPMA)
            {
                sonuc = ilk_sayi * ikinci_sayi;

            }else if(islem_turu == Islem_Turu.BOLME)
            {
                sonuc = ilk_sayi / ikinci_sayi;
            }

            return sonuc;
        }
    }
}
