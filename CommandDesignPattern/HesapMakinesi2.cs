﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern
{
    class HesapMakinesi2
    {
        public static int Islemi_Yap(Islem islem , int ilk_sayi, int ikinci_sayi)
        {
            return islem.Islem(ilk_sayi, ikinci_sayi);
        }
    }
}
