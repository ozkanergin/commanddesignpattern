﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern
{
    public class ToplamaCommand : Islem
    {
        public int Islem(int a, int b)
        {
            return a + b;
        }
    }
}
