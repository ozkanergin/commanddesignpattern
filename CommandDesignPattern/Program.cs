﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern
{
    public enum Islem_Turu
    {
        TOPLAM,
        CIKARMA,
        BOLME,
        CARPMA
    }
    class Program
    {
        static void Main(string[] args)
        {

            BirinciIslem();

            IkinciIslem();

            Console.Read();

        }

        public async static Task BirinciIslem()
        {
            await MakeTeaAsync();

            Console.WriteLine("Make Tea Async fonskiyonu bitti.");

            await Task.Delay(10000);

            Console.WriteLine("Birinci işlem gecikmeli olarak bitti");
        }

        public static void IkinciIslem()
        {
            Console.WriteLine("Ikinci işlem başladı");

            Console.WriteLine("Ikinci işlem bitti");
        }

        public static async Task MakeTeaAsync()
        {
            Console.WriteLine("Take the cups out");

            var water = await BoilWaterAsync();

            Console.WriteLine("Pour water in the cup");
        }

        public static async Task<string> BoilWaterAsync()
        {
            Console.WriteLine("Start the kettle");
            Console.WriteLine("Waiting for the kettle");

            await Task.Delay(2000);

            Console.WriteLine("Kettle finished boiling");

            return "water";
        }
    }
}
